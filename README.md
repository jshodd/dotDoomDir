# dotDoomDir

This is where I'm keeping my Doom Emacs configuration files. I've finally given up on maintaining my own config, and figure I might as well save some time and use a framework. High level changes to Doom features will be changed in `init.el`, any additional packages added in will be in `packages.el`, and lastly any custom configurations like keybindings will be found in `config.el`

First you'll need to install Emacs. Here's how I've taken to doing it lately:

Download the latest version of emacs here: https://ftp.gnu.org/pub/gnu/emacs/. In this example we're installing 27.2.

``` sh
# First we need to modify our source.list and pull down some dependencies
sudo cp /etc/apt/sources.list /etc/apt/sources.list~
sudo sed -Ei 's/^# deb-src /deb-src /' /etc/apt/sources.list
sudo apt-get update
sudo apt-get build-dep emacs

#Next we'll download, extract, and install emacs
wget https://ftp.gnu.org/pub/gnu/emacs/emacs-27.2.tar.gz
tar xzvf emacs-27.2.tar.gz
cd emacs-27.2.tar.gz
./autogen.sh
./configure
make
sudo make install
```


For the Doom setup we'll need to clone the doom repo as well as this one and run the installer:

``` sh
git clone https://gitlab.com/jshodd/dotDoomDir.git ~/.doom.d
git clone https://github.com/hlissner/doom-emacs ~/.emacs.d
~/.emacs.d/bin/doom install
```

remember to run `~/.emacs.d/bin/doom sync` after changing these files and restart emacs for changes to take place. I don't promise that these configurations will change your life, but my objective was to be able to get my editor up and running on any machine with two or three commands and this fits the bill.
